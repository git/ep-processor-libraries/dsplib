@echo off
rem # *************************************************************************
rem #  FILE           : setenv.bat
rem #  DESCRIPTION    :
rem #
rem #     This batch script should be run first before building dsplib
rem #
rem # *************************************************************************

rem # *************************************************************************
rem # *** Set up tools version (which are root directory)
rem # *************************************************************************

set CCSCGTVER=8.2.2
set CCSVERSION=CCSV8.1

set CCSV8_INSTALL_DIR=C:/ti/ccsv8
set CGT_INSTALL_DIR=C:/ti/ccsv8/tools/compiler/ti-cgt-c6000_8.2.2

rem # set dsplib repository path
xs -f setxdcpath.js
if errorlevel 1 goto end

rem # call tempcfg.bat
call tempcfg
rem # remove tempcfg.bat
rm tempcfg.bat
