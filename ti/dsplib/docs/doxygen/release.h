/* ======================================================================== *
 * DSPLIB -- Doxygen generation header file                                 *
 *                                                                          *
 * Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */
  
/**
 *  @mainpage 
 *  <p align="center"> 
 *  <a href="#Introduction">Introduction</a>,
 *  <a href="#Documentation">Documentation</a>,
 *  <a href="#Whats_New">What's New</a>
 *  <a href="#Compatibility">Upgrade and Compatibility Information</a>,
 *  <a href="#Host_Intrinsic_Library_Support">Host Intrinsic Library Support</a>,
 *  <a href="#Device_Support">Device Support</a>,
 *  <a href="#Validation">Validation Information</a>,
 *  <a href="#Known_Issues">Known Issues</a>,
 *  <a href="#Version">Version Information</a>,
 *  <a href="#Support">Technical Support</a>
 *  </p>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Introduction***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *
 *  <hr> 
 *  <h2><a name="General_Info">Introduction</a></h2>
 *  The TI C6000 DSPLIB is an optimized DSP Function Library for C programmers. It includes many
 *  C-callable, optimized, general-purpose signal-processing routines. These routines are typically 
 *  used in computationally-intensive real-time applications where optimal execution speed is critical.
 * 
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Documentation**************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <hr>
 *
 *  <h2><a name="Documentation">Documentation</a></h2>
 *  <p>The following documentation is available: </p> 
 *  - <a href="docs/doxygen/DSPLIB.chm">DSPLIB User's Guide</a>
 *
 *  <p>Release notes from previous releases are also available in the
 *  <a href="docs/relnotes_archive">relnotes_archive</a>
 *  directory.</p>
 *
 *  <a href="#XDC_TOP">back to top</a> 
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*************************************What's New***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *
 *  <h2><a name="Whats_New">What's New</a></h2>
 *  <p> The following new features were added for C66x:  
 *  <ol>
 *    <li> SP/DP Cholesky Complex Matrix Decomposition.
 *    <li> SP/DP LU Matrix Decomposition.
 *    <li> SP/DP LU Complex Matrix Decomposition.
 *    <li> SP/DP LUD Linear Solver.
 *    <li> SP/DP LUD Complex Linear Solver.
 *    <li> SP/DP LUD Matrix Inversion.
 *    <li> SP/DP LUD Complex Matrix Inversion.
 *    <li> SP/DP QR Matrix Decomposition.
 *    <li> SP/DP QR Complex Matrix Decomposition.
 *    <li> SP/DP SV Matrix Decomposition.
 *    <li> SP/DP SV Complex Matrix Decomposition.
 *  </ol>
 *
 *  </p>
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--************************Upgrade and Compatibility Information*************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Compatibility">Upgrade and
 *  Compatibility Information</a></h2>
 *
 *  This <b>ti.dsplib</b> release is not backward compatible with the last released version.  
 *
 *
 *  Please note that the package compatibility keys are independent of XDC product release numbers. 
 * 
 *  Package compatibility keys are intended to: 
 * 
 *   -# Enable tooling to identify incompatibilities between components, and 
 *   -# Convey a level of compatibility between different releases to set end user expectations. 
 * 
 * Package compatibility keys are composed of 4 comma-delimited numbers - M, S, R, P - where: 
 *
 * - <b>M = Major</b> - A difference in M indicates a break in compatibility. The package consumer is required to re-write its source code in order to use the package.
 * - <b>S = Source</b> - A difference in S indicates source compatibility. The package consumerís source code doesn't require change, but does require a recompile. 
 * - <b>R = Radix</b> - A difference in R indicates an introduction of new features, but compatibility with previous interfaces is not broken. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source.
 * - <b>P = Patch</b> - A difference in P indicates that only bugs have been fixed in the latest package and no new features have been introduced. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source. 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*****************************************Device Support*******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Device_Support">Device Support</a></h2>
 *
 *  This release supports the following device families: 
 *
 *  <ul>
 *    <li> C64x+ DSP
 *    <li> C674x DSP
 *    <li> C66x DSP
 *  </ul>
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--********************************Host Intrinsic Library Support************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Host_Intrinsic_Library_Support">Host Intrinsic Library Support</a></h2>
 *
 *  This release supports Host Intrinsic Library. Please refer to 
 *  <a href="http://processors.wiki.ti.com/index.php/Run_Intrinsics_Code_Anywhere">Texas Instruments
 *  Host Intrinsic Library Wiki Page</a> for more information. 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**********************************Validation Information******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *

 *  <hr>
 *
 *  <h2><a name="Validation">Validation Information</a></h2>
 *
 *  This release was built and validated using the following tools:
 *
 *  <ul>
 *    <li> XDC Tools version 3.25.00.48
 *    <li> C6x Code Generation Tools version 7.4.2
 *    <li> CCS 5.4
 *  </ul>
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Known Issues***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Known_Issues">Known Issues</a></h2>
 *
 *  <ul>
 *    <li> DSPLIB package expanded in the graphical editor the information displayed is not correct (IR #081767)
 *  </ul>
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <hr>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************** Versioning **********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <h2><a name="Version">Version Information</a></h2>
 *
 *  This product's version follows a version format, <b>M.m.p.b</b>,
 *  where <b>M</b> is a single digit Major number, <b>m</b> is single digit minor number,
 *  <b>p</b> is a single digit patch number and <b>b</b> is an unrestricted set of digits used as an incrementing build counter. 
 
 *  <p>Please note that version numbers and compatibility keys are
 *  NOT the same. For an explanation of compatibility keys, please refer to
 *  the '<a href="#Compatibility">Upgrade and Compatibility Information</a>' section.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*********************************** Technical Support ********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *  <h2><a name="Support">Technical Support</a></h2>
 *
 *  Questions regarding the DSPLIB library should be directed to the
 *  <a href="http://e2e.ti.com/support/embedded/f/355.aspx">Texas Instruments
 *  BIOS E2E Forum</a>.  Please include the text DSPLIB in the title and add
 *  the DSPLIB tag to your post.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *
 *
 */
