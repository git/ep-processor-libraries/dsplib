/*
 * This is the header file which doxygen parses to generate the main
 * software manifest page
 */

/**
 * @mainpage DSPLIB Bundle Software Manifest
 *
 *  <hr size="2" width="50%" align=left>
 *
 * @section packages Individual Package Software Manifests
 *
 *  - <a href="../../../packages/ti/dsplib/docs/manifest/Software_Manifest.html">DSPLIB Software Manifest</a>                    
 *
 */

/*
 * Copyright � 2010 Texas Instruments Incorporated.  All rights reserved.
 *
 * Information in this document is subject to change without notice.
 * Texas Instruments may have pending patent applications, trademarks,
 * copyrights, or other intellectual property rights covering matter in this
 * document.  The furnishing of this document is given for usage with Texas
 * Instruments products only and does not give you any license to the
 * intellectual property that might be contained within this document.
 * Texas Instruments makes no implied or expressed warranties in this document
 * and is not responsible for the products based from this document.
 *
 *
 */
