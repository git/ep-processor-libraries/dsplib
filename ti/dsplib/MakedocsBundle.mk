# ==============================================================================
# File            : MakedocsBundle.mk
# Description     : 
#
#   GNU makefile to generate DSPLIB bundle documentation using Doxygen
#
# ==============================================================================


# Common paths
DSPLIBPATH = ../../
SWMPATH = docs/manifest
DOXPATH = docs/doxygen
DOXHTML = $(DOXPATH)/html
ECOHTML = $(DOXPATH)/html
SWMHTML = $(SWMPATH)/html

TI_DOXYGEN_TEMPLATES ?= ../docs/doxygen/TI_Templates

# Document targets 
USAGE_DOC          = ./docs/DSPLIB_Users_Manual.chm
MANIFEST_DOC       = ./docs/DSPLIB_Software_Manifest.chm
RELEASE_DOC_BUNDLE = ./$(DOXPATH)/release.chm
#ECLIPSE_DIR  = ./eclipse/plugins

# Redirection script
REDIRECT = $(SWTOOLS_PATH)/redirect.js


#release: $(USAGE_DOC) $(MANIFEST_DOC) $(ECLIPSE_DIR)
release: $(USAGE_DOC) $(MANIFEST_DOC)

releasenotes : $(RELEASE_DOC_BUNDLE)

$(RELEASE_DOC_BUNDLE): ./docs/doxygen/release.h
	-@echo generating Release Notes ...
	if test ! -d ./docs/doxygen/tmp; then mkdir ./docs/doxygen/tmp; fi
	cp $(TI_DOXYGEN_TEMPLATES)/*.* ./docs/doxygen/tmp
	doxygen $(SWTOOLS_PATH)/docs/doxygen/bundlerelDoxyfile
	@xs -f $(SWTOOLS_PATH)/bundlerelease.js
	-@$(RMDIR) ./docs/doxygen/tmp

genbundledocs $(USAGE_DOC): ./$(DOXPATH)/doxygen.h
	-@echo Generating DSPLIB bundle documentation ...
	-@echo copying tirex folder
	cp -r ../.metadata .
	if test ! -d $(DOXHTML); then mkdir $(DOXHTML); fi
	cp $(TI_DOXYGEN_TEMPLATES)/*.* $(DOXHTML)
	@echo Pulling in DSPLIB API Documentation ...
	@cp -fru $(DSPLIBPATH)/dsplib/$(ECOHTML) $(DOXHTML)/dsplib_html
	doxygen ./$(DOXPATH)/Doxyfile
	xs -f $(REDIRECT) ./doxygen/html/index.html > ./docs/DSPLIB_Users_Manual.html

$(MANIFEST_DOC): ./$(SWMPATH)/manifest.h
	-@echo generating Software Manifest ...
	if test ! -d ./$(SWMHTML); then mkdir ./$(SWMHTML); fi
	@cp $(TI_DOXYGEN_TEMPLATES)/*.* ./$(SWMHTML)
	doxygen ./$(SWMPATH)/Doxyfile
	xs -f $(REDIRECT) ./manifest/html/index.html > ./docs/DSPLIB_Software_Manifest.html

#$(ECLIPSE_DIR): ./eclipse/dsplib.xml ./eclipse/dsplib_toc.xml
#	-@echo generating Eclipse Plugin ...
#	xs xdc.tools.eclipsePluginGen -o . -x ./eclipse/dsplib.xml -c ./eclipse/dsplib_toc.xml


# End of Makedocs.mk
