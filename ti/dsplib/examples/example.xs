/* MAIN ROUTINE FROM HERE */
if(arguments.length < 1)
{
  print("USAGE:");
  print("xs -f example.js platform dst\n");
  java.lang.System.exit(1);
}
var target=arguments[0];
var dst=arguments[1];
var repositDir = new java.io.File(dst+"/examples");
  if (!(repositDir.exists() && repositDir.isDirectory())) {
    repositDir.mkdirs();
  }
 
/* Copy Common Files */
/* Copy Module */
var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');

copy.File("./examples/package.xdc", dst+"/examples");

if(target.toString().match("64"))
{
  /* FFT Example for C64x+ & C66x targets */
  copy.File("./examples/fft_ex/fft_example.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x16.h", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x16.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x32.h", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x32.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft32x32.h", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft32x32.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/refOutput.jpg", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/Readme.txt", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/c64P/lnk.cmd", dst+"/examples/fft_ex");
  var prjtarget= "64P";
}
else if (target.toString().match("66"))
{
  /* FFT Example for C64x+ & C66x targets */
  copy.File("./examples/fft_ex/fft_example.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x16.h", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x16.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x32.h", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft16x32.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft32x32.h", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/gen_twiddle_fft32x32.c", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/refOutput.jpg", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/Readme.txt", dst+"/examples/fft_ex");
  copy.File("./examples/fft_ex/c66/lnk.cmd", dst+"/examples/fft_ex");

  /* FFT SP Example for C66x targets */
  copy.File("./examples/fft_sp_ex/c66/lnk.cmd", dst+"/examples/fft_sp_ex");
  copy.File("./examples/fft_sp_ex/fft_example_sp.c", dst+"/examples/fft_sp_ex");
  copy.File("./examples/fft_sp_ex/refOutput.jpg", dst+"/examples/fft_sp_ex");
  copy.File("./examples/fft_sp_ex/Readme.txt", dst+"/examples/fft_sp_ex");

  /* Matrix Examples for C66x targets */
  copy.Directory("./examples/EVD_dp_matrix_ex",dst+"/examples/EVD_dp_matrix_ex");
  copy.Directory("./examples/QRD_dp_least_squares_ex",dst+"/examples/QRD_dp_least_squares_ex");
  copy.Directory("./examples/SVD_dp_compress_ex",dst+"/examples/SVD_dp_compress_ex");
  copy.Directory("./examples/SVD_dp_rank_ex",dst+"/examples/SVD_dp_rank_ex");
  var prjtarget= "66";  
}
else if(target.toString().match("674"))
{
  /* FFT SP Example for C674x targets */
  copy.File("./examples/fft_sp_ex/c674/lnk.cmd", dst+"/examples/fft_sp_ex");
  copy.File("./examples/fft_sp_ex/fft_example_sp.c", dst+"/examples/fft_sp_ex");
  copy.File("./examples/fft_sp_ex/refOutput.jpg", dst+"/examples/fft_sp_ex");
  copy.File("./examples/fft_sp_ex/Readme.txt", dst+"/examples/fft_sp_ex");
  var prjtarget= "674";
}

var tplt = xdc.loadTemplate('ti/dsplib/examples/package.bld.xdt');
tplt.genFile (dst+'/examples/package.bld', null, [target,prjtarget]);

