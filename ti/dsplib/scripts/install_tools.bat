@echo off
:: *************************************************************************
::  FILE           : install_tools.bat
::  DESCRIPTION    :
::
::     This batch script installs all tools required to
::     build a DSPLIB release.
::
:: *************************************************************************

@echo Executing:  %~fn0

set PATH=C:/tools;%PATH%

echo %PATH% > %~dp0/../path.txt

:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
set DRIVE=%~d0
set DSPLIB_WORKING_DIR=%DRIVE%/DSPLIB_Install
set DSPLIB_INSTALL_DIR=%DRIVE%/DSPLIB_Tools
set DSPLIB_INSTALL_DOS_DIR=%DRIVE%\DSPLIB_Tools

:: *************************************************************************
:: *** Specify tool versions
:: *************************************************************************
set XDC_VERSION=3_25_00_48
set CGT_VER_UND=7_4_2
set CGT_VER_DOT=7.4.2
set CC5_VER_UND=5_4_0
set CC5_VER_DOT=5.4.0
set CC5_VER_FUL=5.4.0.00091
set IJM_VERSION=1_2_15
set CYG_VERSION=99-11-01
set PHP_VERSION=5_3_2_1000
set DOX_VERSION=1.5.1-p1
set GVZ_VERSION=2.12
set HHW_VERSION=10-01-2007
set TIT_VERSION=10-01-2007
set EPI_VERSION=20091203
set XDS_VERSION=7_21_01_07
set SWTOOLS_VERSION=5_0_8_0
set MATHLIB_VERSION=3_1_0_0



:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
set XDC_INSTALL_DIR=%DSPLIB_INSTALL_DIR%/xdctools_%XDC_VERSION%
set CC5_INSTALL_DIR=%DSPLIB_INSTALL_DIR%/CCSV%CC5_VER_UND%
set CGT_INSTALL_DIR=%CC5_INSTALL_DIR%/ccsv5/tools/compiler/c6000_%CGT_VER_DOT%
set DOX_INSTALL_DIR=%DSPLIB_INSTALL_DIR%
set IJM_INSTALL_DIR=%DSPLIB_INSTALL_DIR%
set CYG_INSTALL_DIR=%DSPLIB_INSTALL_DIR%
set PHP_INSTALL_DIR=%DSPLIB_INSTALL_DIR%
set EPI_INSTALL_DIR=%DSPLIB_INSTALL_DIR%
set DOX_INSTALL_DOS_DIR=%DSPLIB_INSTALL_DOS_DIR%
set SWTOOLS_INSTALL_DIR=%DSPLIB_WORKING_DIR%
set MATHLIB_INSTALL_DIR=%DRIVE%/ti/mathlib_c66x_%MATHLIB_VERSION%


:: *************************************************************************
:: *** Specify WGET locations
:: *************************************************************************
::  set XDC_WGET_URL="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/rtsc/%XDC_VERSION%/exports"
set XDC_WGET_URL="http://www.sanb.design.ti.com/tisb_releases/XDCtools/%XDC_VERSION%/exports"
set CGT_WGET_URL="http://syntaxerror.dal.design.ti.com/release/releases/c60/rel%CGT_VER_UND%/build/install"
set CC5_WGET_URL="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_ccstudio/CCSv5/CCS_%CC5_VER_UND%/exports"
set DOX_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/doxygen"
set IJM_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/installjammer"
set CYG_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/cygwin"
set PHP_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/php"
set EPI_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/xdc"
set MATHLIB_WGET_URL="http://software-dl.ti.com/sdoemb/sdoemb_public_sw/mathlib/%MATHLIB_VERSION%/exports"

:: *************************************************************************
:: *** Specify WGET packages
:: *************************************************************************
set XDC_WGET_PKG=xdctools_setupwin32_%XDC_VERSION%.exe
set CC5_WGET_PKG=ccs_setup_%CC5_VER_FUL%.exe
set CGT_WGET_PKG=ti_cgt_c6000_%CGT_VER_DOT%_setup_win32.exe
set DOX_WGET_PKG=doxygen.zip
set IJM_WGET_PKG=installjammer_v%IJM_VERSION%.zip
set CYG_WGET_PKG=cygwin-%CYG_VERSION%.zip
set PHP_WGET_PKG=PHP_%PHP_VERSION%.zip
set EPI_WGET_PKG=xdc_eclipse_plugin_gen.zip
set MATHLIB_WGET_PKG=mathlib_c66x_%MATHLIB_VERSION%_Win32.exe



:: *************************************************************************
:: ** These are required for local build without tools support (T: drive)
:: *************************************************************************
set CCSVERSION=CCSV5
set CCSV5_INSTALL_DIR=%CC5_INSTALL_DIR%/ccsv5
set CCSCGTVER=%CGT_VER_DOT%
set C66CODEGENTOOL=%CGT_INSTALL_DIR%
set C64CODEGENTOOL=%CGT_INSTALL_DIR%
set C674CODEGENTOOL=%CGT_INSTALL_DIR%
set TI_DOXYGEN_TEMPLATES=%DOX_INSTALL_DIR%/Doxygen/TI_Templates/%TIT_VERSION%


:: *************************************************************************
:: *************************************************************************
:: ** Start installation process
:: *************************************************************************
:: *************************************************************************
@echo .
@echo #######################################################################
@echo ##  Installing Required Tools
@echo #######################################################################
@echo .

:: *************************************************************************
:: *** Create installation directories
:: *************************************************************************
if not exist "%DSPLIB_WORKING_DIR%" mkdir "%DSPLIB_WORKING_DIR%"
@cd %DSPLIB_WORKING_DIR%

if not exist "%DSPLIB_INSTALL_DIR%" mkdir "%DSPLIB_INSTALL_DIR%"


:: *************************************************************************
:: ** Install XDC
:: *************************************************************************
if exist %XDC_WGET_PKG% goto xdc_install
@echo WGET:  %XDC_WGET_PKG%
wget -nc --no-proxy %XDC_WGET_URL%/%XDC_WGET_PKG%
if not exist %XDC_WGET_PKG% goto xdc_error
:xdc_install
@echo Install:  %XDC_WGET_PKG%
if exist %XDC_INSTALL_DIR% goto xdc_exist
%XDC_WGET_PKG% --prefix %XDC_INSTALL_DIR% --mode silent
:xdc_exist
@echo ......... %XDC_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%/bin;%PATH%



:: *************************************************************************
:: ** Install CCS
:: *************************************************************************
if exist %CC5_WGET_PKG% goto ccs_install
@echo WGET:  CCS%CC5_VER_FUL%_win32.zip
wget -nc --no-proxy %CC5_WGET_URL%/%CC5_WGET_PKG%
mv %CC5_WGET_PKG%* %CC5_WGET_PKG% 
if not exist %CC5_WGET_PKG% goto ccs_error
:ccs_install
@echo Install:  ccs_setup_%CC5_VER_FUL%.exe
if exist %CC5_INSTALL_DIR% goto ccs_exist
ccs_setup_%CC5_VER_FUL%.exe --prefix %CC5_INSTALL_DIR% --mode silent
@cd ..
:ccs_exist
@echo ......... ccs_setup_%CC5_VER_FUL%.exe  Installed
@echo .


:: *************************************************************************
:: ** Install CGT
:: *************************************************************************
if exist %CGT_WGET_PKG% goto cgt_install
@echo WGET:  %CGT_WGET_PKG%
wget -nc --no-proxy %CGT_WGET_URL%/%CGT_WGET_PKG%
if not exist %CGT_WGET_PKG% goto cgt_error
:cgt_install
@echo Install:  %CGT_WGET_PKG%
if exist %CGT_INSTALL_DIR% goto cgt_exist
%CGT_WGET_PKG% --prefix %CGT_INSTALL_DIR% --mode silent
:cgt_exist
@echo ......... %CGT_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Doxygen
:: *************************************************************************
if exist %DOX_WGET_PKG% goto dox_install
@echo WGET:  %DOX_WGET_PKG%
wget -nc --no-proxy %DOX_WGET_URL%/%DOX_WGET_PKG%
if not exist %DOX_WGET_PKG% goto dox_error
:dox_install
@echo Install:  %DOX_WGET_PKG%
if exist %DOX_INSTALL_DIR%/Doxygen goto dox_exist
@unzip -o -d %DOX_INSTALL_DIR% %DOX_WGET_PKG%
:dox_exist
@echo ......... %DOX_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install InstallJammer
:: *************************************************************************
if exist %IJM_WGET_PKG% goto ijm_install
@echo WGET:  %IJM_WGET_PKG%
wget -nc --no-proxy %IJM_WGET_URL%/%IJM_WGET_PKG%
if not exist %IJM_WGET_PKG% goto ijm_error
:ijm_install
@echo Install:  %IJM_WGET_PKG%
if exist %IJM_INSTALL_DIR%/Installjammer goto ijm_exist
@unzip -o -d %IJM_INSTALL_DIR% %IJM_WGET_PKG%
:ijm_exist
@echo ......... %IJM_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Cygwin
:: *************************************************************************
if exist %CYG_WGET_PKG% goto cyg_install
@echo WGET:  %CYG_WGET_PKG%
wget -nc --no-proxy %CYG_WGET_URL%/%CYG_WGET_PKG%
if not exist %CYG_WGET_PKG% goto cyg_error
:cyg_install
@echo Install:  %CYG_WGET_PKG%
if exist %CYG_INSTALL_DIR%/cygwin goto cyg_exist
@unzip -o -d %CYG_INSTALL_DIR% %CYG_WGET_PKG%
:cyg_exist
@echo ......... %CYG_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install PHP
:: *************************************************************************
if exist %PHP_WGET_PKG% goto php_install
@echo WGET:  PHP_%PHP_VERSION%.zip
wget -nc --no-proxy %PHP_WGET_URL%/%PHP_WGET_PKG%
if not exist %PHP_WGET_PKG% goto php_error
:php_install
@echo Install:  %PHP_WGET_PKG%
if exist %PHP_INSTALL_DIR%/PHP_%PHP_VERSION% goto php_exist
@unzip -o -d %PHP_INSTALL_DIR% %PHP_WGET_PKG%
:php_exist
@echo ......... %PHP_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Eclipse Plugin Generator
:: *************************************************************************
if exist %EPI_WGET_PKG% goto epi_install
@echo WGET:  %EPI_WGET_PKG%
wget -nc --no-proxy %EPI_WGET_URL%/%EPI_WGET_PKG%
if not exist %EPI_WGET_PKG% goto epi_error
:epi_install
@echo Install:  %EPI_WGET_PKG%
if exist %EPI_INSTALL_DIR%/xdc_eclipse_plugin_gen goto epi_exist
@unzip -o -d %EPI_INSTALL_DIR% %EPI_WGET_PKG%
:epi_exist
@echo ......... %EPI_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Install MATHLIB
:: *************************************************************************
if exist %MATHLIB_WGET_PKG% goto mathlib_install
@echo WGET:  %MATHLIB_WGET_PKG%
wget -nc --no-proxy %MATHLIB_WGET_URL%/%MATHLIB_WGET_PKG%
ren "%MATHLIB_WGET_PKG%*" "%MATHLIB_WGET_PKG%"
if not exist %MATHLIB_WGET_PKG% goto mathlib_error
:mathlib_install
@echo Install:  %MATHLIB_WGET_PKG%
if exist %MATHLIB_INSTALL_DIR% goto mathlib_exist
%MATHLIB_WGET_PKG% --prefix %MATHLIB_INSTALL_DIR% --mode silent
:mathlib_exist
@echo ......... %MATHLIB_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Install TI-SWTOOLS
:: *************************************************************************
@echo Clone:  TI-SWTOOLS
if exist swtools goto swtools_update
call git clone git://gforge01.dal.design.ti.com/swtools
if not exist swtools goto swtools_error
goto swtools_install
:swtools_update
cd swtools
::call git stash
call git pull origin master
cd ..

:swtools_install
@echo Install:  TI-SWTOOLS
cd swtools
call git checkout DEV_TI_MAS_SWTOOLS_%SWTOOLS_VERSION%
xdc -P %SWTOOLS_INSTALL_DIR%/swtools/ti/mas/swtools
@echo ......... TI-SWTOOLS  Installed
@echo .


@echo #######################################################################
@echo ##  All Required Tools Installed
@echo #######################################################################
@echo .


:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
set PATH=%SystemRoot%;%SystemRoot%/system32
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%/jre/bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\doxygen\%DOX_VERSION%\bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\Graphviz\%GVZ_VERSION%\bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\HTML_Help_Workshop\%HHW_VERSION%;%PATH%
set PATH=%TI_DOXYGEN_TEMPLATES%;%PATH%
set PATH=%IJM_INSTALL_DIR%/InstallJammer/v%IJM_VERSION%;%PATH%
set PATH=%CYG_INSTALL_DIR%/cygwin/bin;%PATH%
set PATH=%PHP_INSTALL_DIR%/PHP_%PHP_VERSION%;%PATH%
set j=1


:: *************************************************************************
:: ** Create XDC environment variables
:: *************************************************************************
set xdc=%XDC_INSTALL_DIR%\xdc.exe $*
set XDCPATH=%CC5_INSTALL_DIR%/ccsv5/packages
set XDCPATH=%XDCPATH%;%EPI_INSTALL_DIR%/xdc_eclipse_plugin_gen/%EPI_VERSION%
set XDCPATH=%XDCPATH%;%XDC_INSTALL_DIR%/packages
set XDCPATH=%XDCPATH%;%SWTOOLS_INSTALL_DIR%/swtools


:: *************************************************************************
:: ** Create environment variables for XDC
:: *************************************************************************
@cd /D %~dp0
@cd ..
xs -f setxdcpath.js
if errorlevel 1 goto clean
call tempcfg
:clean
rm tempcfg.bat
rm path.txt
set INCDIR=%INCDIR%;%MATHLIB_INSTALL_DIR%/packages


:: *************************************************************************
:: ** Show the build environment
:: *************************************************************************
@echo .
@echo #######################################################################
@echo ##  Build Environment Variables (Start)
@echo #######################################################################
@set
@echo #######################################################################
@echo ##  Build Environment Variables (Stop)
@echo #######################################################################
@echo .


:: *************************************************************************
:: ** Errors and cleanup
:: *************************************************************************
goto install_end

:xdc_error
@echo Unable to find XDC installation:  %XDC_WGET_PKG%  exiting...
goto install_end

:cgt_error
@echo Unable to find Codegen Tools installation:  %CGT_WGET_PKG%  exiting...
goto install_end

:mathlib_error
@echo "Unable to find mathlib Tools installation:  %MATHLIB_WGET_PKG%  exiting..."
goto install_end

:ccs_error
@echo Unable to find CCS installation:  %CC5_WGET_PKG%  exiting...
goto install_end

:dox_error
@echo Unable to find Doxygen installation:  %DOX_WGET_PKG%  exiting...
goto install_end

:ijm_error
@echo Unable to find Installjammer installation:  %IJM_WGET_PKG%  exiting...
goto install_end

:cyg_error
@echo Unable to find Cygwin installation:  %CYG_WGET_PKG%  exiting...
goto install_end

:php_error
@echo Unable to find PHP installation:  %PHP_WGET_PKG%  exiting...
goto install_end

:epi_error
@echo Unable to find Eclipse Plugin Generator installation:  %EPI_WGET_PKG%  exiting...
goto install_end



:: *************************************************************************
:: *** Cleanup and return
:: *************************************************************************
:install_end

set XDC_WGET_URL=
set CGT_WGET_URL=
set CC5_WGET_URL=
set DOX_WGET_URL=
set IJM_WGET_URL=
set CYG_WGET_URL=
set PHP_WGET_URL=
set EPI_WGET_URL=
set MATHLIB_WGET_URL=

set CGT_VER_UND=
set CGT_VER_DOT=
set CC5_VER_UND=
set CC5_VER_DOT=
set IJM_VERSION=
set CYG_VERSION=
set PHP_VERSION=
set DOX_VERSION=
set GVZ_VERSION=
set HHW_VERSION=
set TIT_VERSION=
set EPI_VERSION=
set XDS_VERSION=
set MATHLIB_VERSION=

set XDC_WGET_PKG=
set CC5_WGET_PKG=
set CGT_WGET_PKG=
set DOX_WGET_PKG=
set IJM_WGET_PKG=
set CYG_WGET_PKG=
set PHP_WGET_PKG=
set EPI_WGET_PKG=
set MATHLIB_WGET_PKG=


:end
:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************
