@echo off
SETLOCAL EnableDelayedExpansion
:: *************************************************************************
::  FILE           : build_release.bat
::  DESCRIPTION    :
::
::     This batch script creates the full DSPLIB release
::
:: *************************************************************************

@echo Executing:  %~fn0

:: *************************************************************************
:: *** Initialize build environment variables
:: *************************************************************************
set DSPLIB_HOME_DIR=%~dp0\..
set DSPLIB_BUILD_DIR=%DSPLIB_HOME_DIR%\artifacts
set DSPLIB_OUTPUT_DIR=%DSPLIB_BUILD_DIR%\output
set DSPLIB_LOG_DIR=%DSPLIB_BUILD_DIR%\logs
set DSPLIB_TEST_DIR=%DSPLIB_BUILD_DIR%\test
set DSPLIB_FILE_LIST=%DSPLIB_OUTPUT_DIR%\build_targets
set DSPLIB_TEST_LIST=%DSPLIB_BUILD_DIR%\test-result.txt
set XDCOPTIONS=

set DSPLIB_BUILD_C66=0
set DSPLIB_BUILD_C64P=0
set DSPLIB_BUILD_C674=0
set DSPLIB_C64P_TEST_RPT=DSPLIB_C64Px_TestReport.html
set DSPLIB_C64P_SRC_LOG=%DSPLIB_LOG_DIR%\build_C64P_src.log
set DSPLIB_C674_TEST_RPT=DSPLIB_C674x_TestReport.html
set DSPLIB_C674_SRC_LOG=%DSPLIB_LOG_DIR%\build_C674_src.log
set DSPLIB_C66_TEST_RPT=DSPLIB_C66x_TestReport.html
set DSPLIB_C66_SRC_LOG=%DSPLIB_LOG_DIR%\build_C66_src.log

set DSPLIB_TEST_LOG=%DSPLIB_TEST_DIR%\test_dsplib.log
set DSPLIB_C64P_TEST_LOG=%DSPLIB_TEST_DIR%\test_dsplib_C64P.txt
set DSPLIB_C64P_TEST_EXAMPLES_LOG=%DSPLIB_TEST_DIR%\test_example_dsplib_C64P.txt
set DSPLIB_C66_TEST_LOG=%DSPLIB_TEST_DIR%\test_dsplib_C66.txt
set DSPLIB_C66_TEST_EXAMPLES_LOG=%DSPLIB_TEST_DIR%\test_example_dsplib_C66.txt
set DSPLIB_C674_TEST_LOG=%DSPLIB_TEST_DIR%\test_dsplib_C674.txt
set DSPLIB_C674_TEST_EXAMPLES_LOG=%DSPLIB_TEST_DIR%\test_example_dsplib_C674.txt
set DSPLIB_TEST_LOC=C:\DSPLIB_TEST


:: *************************************************************************
:: *** Clear artifacts list if it exists
:: *************************************************************************
if exist "%DSPLIB_FILE_LIST%" (
  del /F "%DSPLIB_FILE_LIST%"
  )
if exist "%DSPLIB_TEST_LIST%" (
  del /F "%DSPLIB_TEST_LIST%"
)


:: *************************************************************************
:: *** Check command line input
:: *************************************************************************
if "%1" == ""      goto build_all
if "%1" == "all"   goto build_all
if "%1" == "C64P"  goto build_C64P
if "%1" == "C674"  goto build_C674
if "%1" == "C66"   goto build_C66

:: USAGE:
echo ERROR: Option "%1" supplied is invalid...
echo .
echo . Usage: %0 [all C64P C674 C66] (defaults to "all")
echo .
echo .     "all"  : Build all targets 
echo .     "C64P" : Build just the C64Px DSPLIB targets 
echo .     "C674" : Build just the C674 DSPLIB targets 
echo .     "C66"  : Build just the C66x DSPLIB targets 
goto end


:: *************************************************************************
:: *** Set environment variables based on "all"
:: *************************************************************************
:build_all
set DSPLIB_BUILD_C66=1
set DSPLIB_BUILD_C64P=1
set DSPLIB_BUILD_C674=1
goto build_process

:: *************************************************************************
:: *** Set environment variables based on a specific target
:: *************************************************************************
:build_C64P
set DSPLIB_BUILD_C64P=1
goto build_process


:: *************************************************************************
:: *** Set environment variables based on a specific target
:: *************************************************************************
:build_C674
set DSPLIB_BUILD_C674=1
goto build_process



:: *************************************************************************
:: *** Set environment variables based on a specific target
:: *************************************************************************
:build_C66
set DSPLIB_BUILD_C66=1
goto build_process


:build_process

:: *************************************************************************
:: *** Create temporary directory for installers 
:: *************************************************************************
if exist "%DSPLIB_BUILD_DIR%" (
  rmdir /S /Q "%DSPLIB_BUILD_DIR%"
)
if exist "%DSPLIB_TEST_LOC%" (
  rmdir /S /Q "%DSPLIB_TEST_LOC%"
)
mkdir "%DSPLIB_BUILD_DIR%"
mkdir "%DSPLIB_OUTPUT_DIR%"
mkdir "%DSPLIB_LOG_DIR%"
mkdir "%DSPLIB_TEST_LOC%"
if not exist "%DSPLIB_TEST_DIR%" (
  mkdir "%DSPLIB_TEST_DIR%"
  cp -r "%SWTOOLS_DIR%\src\runscript" "%DSPLIB_TEST_DIR%\runscript"
)

:build_process_C64P
:: *************************************************************************
:: *** C64Px build process
:: *************************************************************************
if "%DSPLIB_BUILD_C64P%" == "0" goto build_process_C674

:: *************************************************************************
:: *** Build C64Px DSPLIB Source delivery
:: *************************************************************************
cd "%DSPLIB_HOME_DIR%"
xdc clean > "%DSPLIB_C64P_SRC_LOG%"
xdc XDCARGS="c64Px src bundle install" >> "%DSPLIB_C64P_SRC_LOG%" 2>&1

:: *************************************************************************
:: *** Update the build results for Jenkins
:: *************************************************************************
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %DSPLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %DSPLIB_FILE_LIST%
        )
)
:: *************************************************************************
:: *** Copy Installers
:: *************************************************************************
copy "dsplib"*".bin" "%DSPLIB_OUTPUT_DIR%"
copy "dsplib"*".exe" "%DSPLIB_OUTPUT_DIR%"
:: Install the dsplib Win32 installer for testing
FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c64Px_*_Win32.exe') DO %%A --prefix "%DSPLIB_TEST_LOC%\DSPLIB_C64P" --mode silent

:: Test DSPLIB C64x+ release
@echo Verifying DSPLIB C64x+ release...
cd "%DSPLIB_TEST_DIR%\runscript"

xdc > "%DSPLIB_TEST_LOG%" 2>&1

:: Build C64x+ Kernel projects
xs -f "%SWTOOLS_DIR%"\BuildCCSProjects.xs  %DSPLIB_TEST_LOC%\DSPLIB_C64P\packages batch.bat Release  
call batch.bat >> "%DSPLIB_TEST_LOG%" 2>&1

:: Build C64x+ Example projects
xs -f "%SWTOOLS_DIR%"\BuildCCSProjects.xs  %DSPLIB_TEST_LOC%\DSPLIB_C64P\examples batch.bat Release
call batch.bat >> "%DSPLIB_TEST_LOG%" 2>&1

:: Create test suite for C64x+ kernels
xs -f "%SWTOOLS_DIR%"\createRegressXml.xs  %DSPLIB_TEST_LOC%\DSPLIB_C64P\packages dsplib_c64p_test.xml  

:: Create test suite for C64x+ examples
xs -f "%SWTOOLS_DIR%"\createRegressXml.xs  %DSPLIB_TEST_LOC%\DSPLIB_C64P\examples dsplib_c64p_test_examples.xml  

:: Execute C64x+ kernel test suite
call run regress.xs dsplib_c64p_test.xml -n 1 -v >> "%DSPLIB_C64P_TEST_LOG%" 2>&1
:: Execute C64x+ example test suite
call run regress.xs dsplib_c64p_test_examples.xml -n 1 -v >> "%DSPLIB_C64P_TEST_EXAMPLES_LOG%" 2>&1


:: *************************************************************************
:: *** Generate C64Px Test report
:: *************************************************************************
cd "%DSPLIB_HOME_DIR%"

xs -f scripts\genTestReport.js "%DSPLIB_C64P_TEST_LOG%" "%DSPLIB_OUTPUT_DIR%"
copy "%DSPLIB_OUTPUT_DIR%"\"%DSPLIB_C64P_TEST_RPT%" "%DSPLIB_TEST_DIR%"\"%DSPLIB_C64P_TEST_RPT%"

:: *************************************************************************
:: *** Skipping testing for linux installers
:: *************************************************************************
FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c64Px_*_Linux.bin') do (
       @echo %%A.bin:SKIPPED: >> %DSPLIB_TEST_LIST%
       )


:: *************************************************************************
:: *** Display Test Result
:: *************************************************************************

FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c64Px_*_Win32.exe') do (
       @findstr /b "Verified" "%DSPLIB_C64P_TEST_LOG%" > test.log
       set /p RESULT= <test.log
       del /F test.log
             if "!RESULT!"=="Verified" (
               @echo %%A.exe:PASSED:%DSPLIB_C64P_TEST_RPT% >> %DSPLIB_TEST_LIST%
			   @echo .
               @echo #######################################################################
               @echo ##  C64x+ Test-Verification: PASSED
               @echo #######################################################################
             ) else (
			   @echo %%A.exe:FAILED: %DSPLIB_C64P_TEST_RPT% >> %DSPLIB_TEST_LIST%
			   @echo .
               @echo #######################################################################
               @echo ##  C64x+ Test-Verification: FAILED
               @echo #######################################################################
			 )
	   )
:: ************************************************************************
:: ************************** Webgen **************************************
:: ************************************************************************

:: Copy Webgen folder 
if exist "%DSPLIB_BUILD_DIR%\webgen" (
  del /F "%DSPLIB_BUILD_DIR%\webgen"
)
cp -r "%DSPLIB_HOME_DIR%\webgen" "%DSPLIB_BUILD_DIR%\webgen"

:: Copy Manifest
cp -f docs\manifest\Software_Manifest.html  "%DSPLIB_OUTPUT_DIR%\Software_Manifest.html"


:: Copy Function reference
cp -f docs\doxygen\DSPLIB.chm  "%DSPLIB_OUTPUT_DIR%\DSPLIB.chm"



:build_process_C674
:: *************************************************************************
:: *** C674x build process
:: *************************************************************************
if "%DSPLIB_BUILD_C674%" == "0" goto build_process_C66

:: *************************************************************************
:: *** Reset Environment
:: *************************************************************************
cd %~dp0
call install_tools.bat


:: *************************************************************************
:: *** Build C674x DSPLIB Source delivery
:: *************************************************************************
cd "%DSPLIB_HOME_DIR%"
xdc clean > "%DSPLIB_C674_SRC_LOG%"
xdc XDCARGS="c674x src bundle install" >> "%DSPLIB_C674_SRC_LOG%" 2>&1

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %DSPLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %DSPLIB_FILE_LIST%
        )
)
copy "dsplib"*".bin" "%DSPLIB_OUTPUT_DIR%"
copy "dsplib"*".exe" "%DSPLIB_OUTPUT_DIR%"


:: Install the dsplib Win32 installer for testing
FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c674x_*_Win32.exe') DO %%A --prefix "%DSPLIB_TEST_LOC%\DSPLIB_C674" --mode silent

:: Test DSPLIB C674x release
@echo Verifying DSPLIB C674x release...
cd "%DSPLIB_TEST_DIR%\runscript"

xdc > "%DSPLIB_TEST_LOG%" 2>&1

:: Build C674x kernel projects
xs -f "%SWTOOLS_DIR%"\BuildCCSProjects.xs  %DSPLIB_TEST_LOC%\DSPLIB_C674\packages batch.bat Release  
call batch.bat >> "%DSPLIB_TEST_LOG%" 2>&1

:: Build C674x Example projects
xs -f "%SWTOOLS_DIR%"\BuildCCSProjects.xs  %DSPLIB_TEST_LOC%\DSPLIB_C674\examples batch.bat Release
call batch.bat >> "%DSPLIB_TEST_LOG%" 2>&1


:: Create test suite for C674x kernels
xs -f "%SWTOOLS_DIR%"\createRegressXml.xs  %DSPLIB_TEST_LOC%\DSPLIB_C674\packages dsplib_c674_test.xml  

:: Create test suite for C674x examples
xs -f "%SWTOOLS_DIR%"\createRegressXml.xs  %DSPLIB_TEST_LOC%\DSPLIB_C674\examples dsplib_c674_test_examples.xml  

:: Execute C674x kernel test suite
call run regress.xs dsplib_c674_test.xml -n 1 -v >> "%DSPLIB_C674_TEST_LOG%" 2>&1
:: Execute C674x example test suite
call run regress.xs dsplib_c674_test_examples.xml -n 1 -v >> "%DSPLIB_C674_TEST_EXAMPLES_LOG%" 2>&1


cd "%DSPLIB_HOME_DIR%"

xs -f scripts\genTestReport.js "%DSPLIB_C674_TEST_LOG%" "%DSPLIB_OUTPUT_DIR%"
copy "%DSPLIB_OUTPUT_DIR%"\"%DSPLIB_C674_TEST_RPT%" "%DSPLIB_TEST_DIR%"\"%DSPLIB_C674_TEST_RPT%"

FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c674x_*_Linux.bin') do (
       @echo %%A.bin:SKIPPED: >> %DSPLIB_TEST_LIST%
       )

:: *************************************************************************
:: *** Display Test Result
:: *************************************************************************

FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c674x_*_Win32.exe') do (
       @findstr /b "Verified" "%DSPLIB_C674_TEST_LOG%" > test.log
       set /p RESULT= <test.log
       del /F test.log
             if "!RESULT!"=="Verified" (
               @echo %%A.exe:PASSED:%DSPLIB_C674_TEST_RPT% >> %DSPLIB_TEST_LIST%
			   @echo .
               @echo #######################################################################
               @echo ##  C674x Test-Verification: PASSED
               @echo #######################################################################
             ) else (
			   @echo %%A.exe:FAILED: %DSPLIB_C674_TEST_RPT% >> %DSPLIB_TEST_LIST%
			   @echo .
               @echo #######################################################################
               @echo ##  C674x Test-Verification: FAILED
               @echo #######################################################################
			 )
	   )       
         
  

:build_process_C66
:: *************************************************************************
:: *** C66x build process
:: *************************************************************************
if "%DSPLIB_BUILD_C66%" == "0" goto end

:: *************************************************************************
:: *** Reset Environment
:: *************************************************************************
cd %~dp0
call install_tools.bat


:: *************************************************************************
:: *** Build C66x DSPLIB Source delivery
:: *************************************************************************
cd "%DSPLIB_HOME_DIR%"
xdc clean > "%DSPLIB_C66_SRC_LOG%"
xdc XDCARGS="c66x src bundle install" >> "%DSPLIB_C66_SRC_LOG%" 2>&1

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %DSPLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %DSPLIB_FILE_LIST%
        )
)
copy "dsplib"*".bin" "%DSPLIB_OUTPUT_DIR%"
copy "dsplib"*".exe" "%DSPLIB_OUTPUT_DIR%"


:: Install the dsplib Win32 installer for testing
FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c66x_*_Win32.exe') DO %%A --prefix "%DSPLIB_TEST_LOC%\DSPLIB_C66" --mode silent

:: Test DSPLIB C66x release
@echo Verifying DSPLIB C66x release...
cd "%DSPLIB_TEST_DIR%\runscript"

xdc > "%DSPLIB_TEST_LOG%" 2>&1

:: Build C66x Kernel projects
xs -f "%SWTOOLS_DIR%"\BuildCCSProjects.xs  %DSPLIB_TEST_LOC%\DSPLIB_C66\packages batch.bat Release  
call batch.bat >> "%DSPLIB_TEST_LOG%" 2>&1

:: Build C66x Example projects
xs -f "%SWTOOLS_DIR%"\BuildCCSProjects.xs  %DSPLIB_TEST_LOC%\DSPLIB_C66\examples batch.bat Release
call batch.bat >> "%DSPLIB_TEST_LOG%" 2>&1


:: Create test suite for C66x examples
xs -f "%SWTOOLS_DIR%"\createRegressXml.xs  %DSPLIB_TEST_LOC%\DSPLIB_C66\packages dsplib_c66_test.xml  

:: Create test suite for C66x examples
xs -f "%SWTOOLS_DIR%"\createRegressXml.xs  %DSPLIB_TEST_LOC%\DSPLIB_C66\examples dsplib_c66_test_examples.xml  

:: Execute C66x kernel test suite
call run regress.xs dsplib_c66_test.xml -n 1 -v >> "%DSPLIB_C66_TEST_LOG%" 2>&1
:: Execute C66x example test suite
call run regress.xs dsplib_c66_test_examples.xml -n 1 -v >> "%DSPLIB_C66_TEST_EXAMPLES_LOG%" 2>&1


cd "%DSPLIB_HOME_DIR%"

xs -f scripts\genTestReport.js "%DSPLIB_C66_TEST_LOG%" "%DSPLIB_OUTPUT_DIR%"
copy "%DSPLIB_OUTPUT_DIR%"\"%DSPLIB_C66_TEST_RPT%" "%DSPLIB_TEST_DIR%"\"%DSPLIB_C66_TEST_RPT%"

FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c66x_*_Linux.bin') do (
       @echo %%A.bin:SKIPPED: >> %DSPLIB_TEST_LIST%
       )

:: *************************************************************************
:: *** Display Test Result
:: *************************************************************************

FOR /F "tokens=1* delims=." %%A IN ('dir /b /a-d dsplib_c66x_*_Win32.exe') do (
       @findstr /b "Verified" "%DSPLIB_C66_TEST_LOG%" > test.log
       set /p RESULT= <test.log
       del /F test.log
             if "!RESULT!"=="Verified" (
               @echo %%A.exe:PASSED:%DSPLIB_C66_TEST_RPT% >> %DSPLIB_TEST_LIST%
			   @echo .
               @echo #######################################################################
               @echo ##  C66x Test-Verification: PASSED
               @echo #######################################################################
             ) else (
			   @echo %%A.exe:FAILED:%DSPLIB_C66_TEST_RPT% >> %DSPLIB_TEST_LIST%
			   @echo .
               @echo #######################################################################
               @echo ##  C66x Test-Verification: FAILED
               @echo #######################################################################
			 )
	   )            
         
  

:: *************************************************************************
:: *** Cleanup and return
:: *************************************************************************
:end

cd "%DSPLIB_HOME_DIR%"
set DSPLIB_HOME_DIR=
set DSPLIB_BUILD_DIR=
set DSPLIB_OUTPUT_DIR=
set DSPLIB_LOG_DIR=
set DSPLIB_TEST_DIR=
set DSPLIB_C64P_TEST_RPT=
set DSPLIB_C66_TEST_RPT=
set DSPLIB_C674_TEST_RPT=
set DSPLIB_BUILD_C64=
set DSPLIB_BUILD_C66=
set DSPLIB_BUILD_C674=
set DSPLIB_C64P_SRC_LOG=
set DSPLIB_C64P_OBJ_LOG=
set DSPLIB_C66_SRC_LOG=
set DSPLIB_C66_OBJ_LOG=
set DSPLIB_C674_SRC_LOG=
set DSPLIB_C674_OBJ_LOG=
set XDCOPTIONS=
ENDLOCAL
:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************

