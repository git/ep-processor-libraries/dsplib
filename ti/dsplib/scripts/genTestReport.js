function usage()
{
     print("\n\tUSAGE:\n");   
     print("\t\txs -f genTestReport.js raw.txt dst\n");
	 java.lang.System.exit(1);
}
if(arguments.length < 2)
{
   usage();
}

var rawText=arguments[0];
var dst = arguments[1];
var fileIndx = rawText.toString().lastIndexOf("\\");
var fileName = rawText.substr(fileIndx,rawText.length);    
var temp = fileName.toString().split("_");
var libName = temp[1].toString().toUpperCase();
var target = String(temp[2]);
    target = target.substring(0,target.indexOf("."))+"x";
var testReportName = libName+"_"+target+"_TestReport.html";
var versionMod = xdc.module("ti."+libName.toLowerCase()+ ".Version");
var Version = versionMod.MAJOR+"."+versionMod.MINOR+"."+versionMod.PATCH+"."+versionMod.BUILD;      
var tplt = xdc.loadTemplate("test_report.html.xdt");
    tplt.genFile(dst+'\\'+testReportName,null, [libName,target,Version,rawText]);  

