Instructions to build DSPLIB from source:

1. Clone DSPLIB from git://git.ti.com/ep-processor-libraries/dsplib.git 
   Clone SWTOOLS from git://git.ti.com/ep-processor-libraries/swtools.git

2. Make sure following Proc-SDK components are installed:
   - SYSBIOS
   - XDC
   - Code Gen Tools
   - CCS
   - MATHLIB

3. Setup following environment variables (exact version numbers may be different):
export SYSBIOS_INSTALL_DIR="<COMPONENTS_INSTALLATION_ROOT>/bios_6_52_00_12"
export XDC_INSTALL_DIR="<COMPONENTS_INSTALLATION_ROOT>/xdctools_3_50_03_33_core/"
export XDCCGROOT="<COMPONENTS_INSTALLATION_ROOT>/ti-cgt-c6000_8.2.2"
export CGTROOT="<COMPONENTS_INSTALLATION_ROOT>/ti-cgt-c6000_8.2.2"
export C64PCODEGENTOOL="${CGTROOT}"
export C674CODEGENTOOL="${CGTROOT}"
export C66CODEGENTOOL="${CGTROOT}"

export CCSVERSION="CCSV8"
export CCS_INSTALL_DIR="<COMPONENTS_INSTALLATION_ROOT>/ccsv8"

export DSPLIB_ROOT="<DSPLIB git clone root>"
export MATHLIB_INSTALL_DIR="<COMPONENTS_INSTALLATION_ROOT>/mathlib_c66x_3_1_2_2/packages"

export SWTOOLS_INSTALL_DIR="<SWTOOLS git clone root>"
export SWTOOLS_PATH="<SWTOOLS git clone root>/ti/mas/swtools"
export XDCBUILDCFG="${SWTOOLS_PATH}/config.bld"
export INCDIR="${MATHLIB_INSTALL_DIR}/"

export PATH="${XDC_INSTALL_DIR}:${PATH}"

export XDCPATH="${XDCCGROOT}/include;${XDC_INSTALL_DIR}/packages;${SWTOOLS_INSTALL_DIR};${MATHLIB_INSTALL_DIR};${DSPLIB_ROOT};${SYSBIOS_INSTALL_DIR}/packages;"

4. Build SWTOOLS and then DSPLIB
   - Go to directory <SWTOOLS_PATH> and issue command "xdc"
   - Go to directory <DSPLIB_ROOT>/ti/dsplib and issue one of following commands:
       - xdc XDCARGS="c66x bundle"
       - xdc XDCARGS="c674x bundle"
       - xdc XDCARGS="c64Px bundle"


